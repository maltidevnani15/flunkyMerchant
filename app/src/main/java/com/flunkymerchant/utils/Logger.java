package com.flunkymerchant.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.flunkymerchant.BuildConfig;
import com.flunkymerchant.R;


public class Logger {
    private static String TAG = "EventApp";

    public static void setTag(String TAG) {
        Logger.TAG = TAG;
    }

    /**
     * Log.e()
     *
     * @param message which is display in logcat.
     */
    public static void e(String message) {
        if (isDebugMode())
            Log.e(Logger.TAG, message);
    }

    /**
     * Log.v()
     *
     * @param message which is display in logcat.
     */
    public static void v(String message) {
        if (isDebugMode())
            Log.v(Logger.TAG, message);
    }

    /**
     * Log.w()
     *
     * @param message which is display in logcat.
     */
    public static void w(String message) {
        if (isDebugMode())
            Log.w(Logger.TAG, message);
    }

    /**
     * Log.d()
     *
     * @param message which is display in logcat.
     */
    public static void d(String message) {
        if (isDebugMode())
            Log.d(Logger.TAG, message);
    }

    /**
     * Log.i()
     *
     * @param message which is display in logcat.
     */
    public static void i(String message) {
        if (isDebugMode())
            Log.i(Logger.TAG, message);
    }


    /**
     * Show Snack Bar
     *
     * @param context Application/Activity context
     * @param message Message which is display in toast.
     */
    public static void showSnackBar(Context context, String message) {
        if (context != null && !TextUtils.isEmpty(message)) {
            final Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
            final View view = snackbar.getView();
            final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();

            view.setLayoutParams(params);
            snackbar.show();
        }
    }

    /**
     * Show toast
     *
     * @param context Application/Activity context
     * @param message Message which is display in toast.
     */
    public static void toast(Context context, String message) {
        if (context != null && !TextUtils.isEmpty(message)) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Show Default dialog.
     *
     * @param context Application/Activity Context for creating dialog.
     * @param title   Title of dialog
     * @param message Message of dialog
     */
    public static void dialog(Context context, String title, String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.AppTheme_AlertDialog);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(android.R.string.ok), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        if (!dialog.isShowing())
            dialog.show();
    }

    /**
     * Show default dialog
     *
     * @param context Application/Activity Context
     * @param message Message of dialog
     */
    public static void dialog(Context context, String message) {
        dialog(context, context.getString(R.string.app_name), message);
    }

    /**
     * dismiss progress dialog if is visible.
     *
     * @param progressDialog
     */
    public static void dismissProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /**
     * Display progress dialog
     *
     * @param context
     * @return ProgressDialog
     */
    public static ProgressDialog showProgressDialog(Context context) {
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.AppTheme_ProgressDialog_Theme);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }





    private static boolean isDebugMode() {
        return BuildConfig.DEBUG;
    }
}
