package com.flunkymerchant.utils;

import java.util.Arrays;
import java.util.Collection;

public class Constant {
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final int REQUEST_CODE_ASK_PERMISSIONS = 10001;
    public static final int REQUEST_CHECK_SETTINGS = 1111;

    public static final String APP_KEY="flunkyapp";
    public static final String OS="and";
    public static final String EVENTS_LIST="event_list";
    public static final String COUPAN_PRICE="coupan_price";
    public static final String User_id= "user_id";
    public static final String KEY_DATA = "res_object";
    public static final String KEY_USER_DATA="user_data";

    public static final String KEY_STATUS = "res_code";
    public static final String KEY_MESSAGE = "res_message";

    public static final String API_DATE_TIME_FORMATTER = "yyyy-MM-dd";
    public static final String EXPECTED_DATE_FORMATTER = "dd/MM/yyyy";

    public static final String GCM_UNIQUE_ID = "gcm_unique_id";
    public static final String CLIENT_ID_SANDBOX="AXi5T_RhRSvJ7SdDkKX0t24RyOu1Nzmu_14yu3ToEwCp7dc_1w29JnHBUAlhJMDYnZELf0bll8XaCliK";
    public static final String CLIENT_ID_PRODUCTION="Ac51h6pZyomaCuWt9vm0UrKNs10gothkNCLGcPOa_R5C5EAogWdVkZjryNJcJdOy1jZJc3aOe6eBILrB";

    public static final Collection<String> permissions = Arrays.asList("public_profile", "email");

}
