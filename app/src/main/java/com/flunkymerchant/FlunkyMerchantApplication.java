package com.flunkymerchant;

import android.Manifest;
import android.app.Application;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.flunkymerchant.webservices.RestClient;
import com.nostra13.universalimageloader.cache.memory.impl.FIFOLimitedMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class FlunkyMerchantApplication extends Application{
    private static FlunkyMerchantApplication instance;


    private ImageLoaderConfiguration config;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    public static FlunkyMerchantApplication getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initImageLoader();



        new RestClient();
    }

    private void initImageLoader() {
        config = new ImageLoaderConfiguration.Builder(getApplicationContext()).threadPoolSize(10).threadPriority(Thread.MIN_PRIORITY + 2).memoryCacheSize(2500000)
                .memoryCache(new FIFOLimitedMemoryCache(2400000)).memoryCacheSize(2 * 1024 * 1024).build();
        imageLoader.init(config);
    }






    public ImageLoader getImageLoader() {
        return imageLoader;
    }


}
