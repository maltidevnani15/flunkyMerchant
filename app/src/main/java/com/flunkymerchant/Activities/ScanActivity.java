package com.flunkymerchant.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.flunkymerchant.R;

import github.nisrulz.qreader.QRDataListener;
import github.nisrulz.qreader.QREader;

public class ScanActivity extends AppCompatActivity implements View.OnClickListener, SurfaceHolder.Callback, Animation.AnimationListener {
    private SurfaceView mySurfaceView;
    private SurfaceHolder surfaceHolder;
    private View scanView;
    private QREader qrEader;
    private ImageView activity_scan_iv_close;
    Animation Bottom;
    Animation Top;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        Log.e("dfsdf","asds");
        mySurfaceView = (SurfaceView) findViewById(R.id.cameraViewww);
        activity_scan_iv_close=(ImageView)findViewById(R.id.activity_scan_iv_close);
        surfaceHolder = mySurfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        scanView=findViewById(R.id.scan_view);
         Bottom = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        Top=AnimationUtils.loadAnimation(this,R.anim.slide_down);
        scanView.startAnimation(Bottom);
        activity_scan_iv_close.setOnClickListener(this);
        Bottom.setAnimationListener(this);
        Top.setAnimationListener(this);

        qrEader = new QREader.Builder(this, mySurfaceView, new QRDataListener() {
            @Override
            public void onDetected(final String data) {
                Log.e("QREader", "Value : " + data);
                mySurfaceView.post(new Runnable() {
                    @Override
                    public void run() {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result",data);
                        setResult(Activity.RESULT_OK,returnIntent);

                        finish();

                    }
                });
            }
        }).facing(QREader.BACK_CAM)
                .enableAutofocus(true)
                .height(mySurfaceView.getHeight())
                .width(mySurfaceView.getWidth())
                .build();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_scan_iv_close:
                finish();
                break;
        }

    }
    @Override
    protected void onResume() {
        super.onResume();
        qrEader.initAndStart(mySurfaceView);

    }
    @Override
    protected void onPause() {
        super.onPause();
        qrEader.releaseAndCleanup();

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(animation== Bottom){
        scanView.startAnimation(Top);
        }else{
            scanView.startAnimation(Bottom);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
