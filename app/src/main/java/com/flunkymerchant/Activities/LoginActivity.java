package com.flunkymerchant.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flunkymerchant.Models.ResponseOfAllApiModel;
import com.flunkymerchant.R;
import com.flunkymerchant.utils.Constant;
import com.flunkymerchant.utils.Logger;
import com.flunkymerchant.utils.Utils;
import com.flunkymerchant.webservices.RestClient;
import com.flunkymerchant.webservices.RetrofitCallback;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit2.Call;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private TextView login_tv_submit;
    private EditText login_et_email;
    private EditText login_et_password;

    @Override
    protected void initView() {
        login_et_email = (EditText) findViewById(R.id.login_et_email);
        login_et_password = (EditText) findViewById(R.id.login_et_pwd);
        login_tv_submit = (TextView) findViewById(R.id.login_tv_submit);
        login_tv_submit.setOnClickListener(this);

    }

    @Override
    protected void initToolBar() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }


    @Override
    public void onClick(View v) {

        int id = v.getId();
        switch (id) {
          
            case R.id.login_tv_submit:
                Utils.hideSoftKeyboard(this);
                validateData();
                break;
          
        }
    }

    private void validateData() {

        final String email = login_et_email.getText().toString();
        final String password = login_et_password.getText().toString();
        if (!TextUtils.isEmpty(email) && Utils.isEmailValid(email)) {
            if (!TextUtils.isEmpty(password)) {
                doLogin(email,password);
//
            } else {
                Logger.showSnackBar(this,getString(R.string.enter_password));
            }
        } else {
            Logger.showSnackBar(this, getString(R.string.enter_email));
        }
    }

    private void doLogin(String email, String password) {
        Call<ResponseOfAllApiModel>doLogin = new RestClient().getApiInterface().doLogin(email,password,Constant.APP_KEY,"dsfsdfsd","Dasdsdfsdf","and" );
        doLogin.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                Utils.storeString(LoginActivity.this, Constant.KEY_USER_DATA, new Gson().toJson(data.getUserResponseModel()));
                Logger.toast(LoginActivity.this, "Wlecome to flunky");
                Intent i = new Intent(LoginActivity.this,MainActivity.class);
                navigateToNextActivity(i, false);
                if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CAMERA)== PackageManager.PERMISSION_DENIED)
                {
                    ActivityCompat.requestPermissions(LoginActivity.this, new String[] {Manifest.permission.CAMERA},10001);
                }
            }
        });
    }
}
