package com.flunkymerchant.Models;


import com.google.gson.annotations.SerializedName;

public class ResponseOfAllApiModel {
    @SerializedName("user_details")
    private UserResponseModel userResponseModel;

    @SerializedName("voucher_details")
    private UserVoucherDetails userVoucherDetails;

    public UserVoucherDetails getUserVoucherDetails() {
        return userVoucherDetails;
    }

    public void setUserVoucherDetails(UserVoucherDetails userVoucherDetails) {
        this.userVoucherDetails = userVoucherDetails;
    }


    public UserResponseModel getUserResponseModel() {
        return userResponseModel;
    }

    public void setUserResponseModel(UserResponseModel userResponseModel) {
        this.userResponseModel = userResponseModel;
    }



}
