
package com.flunkymerchant.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;

import com.flunkymerchant.FlunkyMerchantApplication;
import com.flunkymerchant.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

public class AppImageView extends FrameLayout {

    private ImageView imageView;
    private ImageView placeHolderImageView;
    private ProgressBar progressBar;
    private Context context;
    private OnLoadBitmap onLoadBitmap;
    private boolean isCenterCrop = true;
    private ImageLoader imageLoader;
    private LayoutParams placeHolderParams;
    private LayoutParams progressParams;
    private boolean hasBitmap = false;
    private boolean isCircleImage = true;
    private boolean isGradient = false;
    private LayoutParams imageParams;

    public void setOnLoadBitmap(OnLoadBitmap onLoadBitmap) {
        this.onLoadBitmap = onLoadBitmap;
    }

    public AppImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public AppImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AppImageView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context c) {
        this.context = c;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        initView();

        addView(placeHolderImageView, placeHolderParams);
        addView(imageView, imageParams);
        addView(progressBar, progressParams);
        progressBar.setVisibility(GONE);
    }


    public void setBitmap(Bitmap bitmap) {
        removeView(progressBar);
        removeView(placeHolderImageView);
        hasBitmap = true;
        if (isCircleImage) {

        } else {
            imageView.setImageBitmap(bitmap);
        }


    }

    public void setImageScaleType(ScaleType scaleType) {
        imageView.setScaleType(scaleType);
    }

    public interface OnLoadBitmap {
        void loadBitmap(Bitmap bitmap);
    }

    public void setCenterCrop(boolean centerCrop) {
        isCenterCrop = centerCrop;
    }


    public void loadImage(String url) {
        if (!TextUtils.isEmpty(url)) {
            imageLoader = FlunkyMerchantApplication.getInstance().getImageLoader();
            imageLoader.displayImage(url, imageView, getImageOptions(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    super.onLoadingComplete(imageUri, view, bitmap);

                    if (isCenterCrop)
                        imageView.setScaleType(ScaleType.CENTER_CROP);

                    setBitmap(bitmap);

                    if (onLoadBitmap != null)
                        onLoadBitmap.loadBitmap(bitmap);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    super.onLoadingFailed(imageUri, view, failReason);
                    removeView(progressBar);
                }

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    super.onLoadingStarted(imageUri, view);
                    progressBar.setVisibility(View.VISIBLE);
                }
            });
        } else {
            removeView(progressBar);
        }
    }

    private DisplayImageOptions getImageOptions() {
        return new DisplayImageOptions.Builder().cacheOnDisk(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.NONE_SAFE).bitmapConfig(Bitmap.Config.ARGB_8888)
                .considerExifParams(true)
                .build();
    }



    public void setImage(int id) {
        imageView.setAdjustViewBounds(true);
        final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), id);
        imageView.setImageBitmap(bitmap);
    }

    public void setImage(String path) {
        imageView.setAdjustViewBounds(true);
        final Bitmap bitmap = BitmapFactory.decodeFile(path);
        setBitmap(bitmap);
    }

    public void setImage(Drawable id) {
        imageView.setAdjustViewBounds(true);
        imageView.setImageDrawable(id);
    }

    public void reset(boolean isPlaceHolder) {
        if (hasBitmap) {

            clearView();
            initView();

            if (!isPlaceHolder)
                addView(placeHolderImageView, placeHolderParams);

            addView(imageView, imageParams);
            addView(progressBar, progressParams);
            progressBar.setVisibility(GONE);
            hasBitmap = false;
        }
    }

    public void setCircleImage(boolean circleImage) {
        isCircleImage = circleImage;
    }

    public void setGradient(boolean gradient) {
        isGradient = gradient;
    }

    private void initView() {
        imageView = new ImageView(context);
        imageView.setAdjustViewBounds(true);

        placeHolderImageView = new ImageView(context);
        placeHolderImageView.setImageResource(R.drawable.img_place_holder);

        progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleSmall);
        progressBar.setIndeterminate(true);

        imageParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        imageParams.gravity = Gravity.CENTER;

        placeHolderParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        placeHolderParams.gravity = Gravity.CENTER;

        progressParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        progressParams.gravity = Gravity.CENTER;
    }

    private void clearView() {
        removeAllViews();
        imageView = null;
        progressBar = null;
        placeHolderImageView = null;
    }
}
