package com.flunkymerchant.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.flunkymerchant.R;


public class AppRadioButton extends RadioButton {

    public AppRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode())
            return;

        final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.componentStyle);
        final String ttfName = ta.getText(0).toString();

        final Typeface font = Typeface.createFromAsset(context.getAssets(), ttfName);
        setTypeface(font);
        ta.recycle();

    }

}
